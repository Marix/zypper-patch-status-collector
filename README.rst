=============================
Zypper Patch Status Collector
=============================

This queries the current patch status of the system from Zypper and exports it in a format compatible with the `Prometheus Node Exporter's`_ textfile collector.

Usage
-----

::

    # HELP zypper_applicable_patches The current count of applicable patches
    # TYPE zypper_applicable_patches gauge
    zypper_applicable_patches{category="security",severity="critical"} 0
    zypper_applicable_patches{category="security",severity="important"} 2
    zypper_applicable_patches{category="security",severity="moderate"} 0
    zypper_applicable_patches{category="security",severity="low"} 0
    zypper_applicable_patches{category="security",severity="unspecified"} 0
    zypper_applicable_patches{category="recommended",severity="critical"} 0
    zypper_applicable_patches{category="recommended",severity="important"} 0
    zypper_applicable_patches{category="recommended",severity="moderate"} 0
    zypper_applicable_patches{category="recommended",severity="low"} 0
    zypper_applicable_patches{category="recommended",severity="unspecified"} 0
    zypper_applicable_patches{category="optional",severity="critical"} 0
    zypper_applicable_patches{category="optional",severity="important"} 0
    zypper_applicable_patches{category="optional",severity="moderate"} 0
    zypper_applicable_patches{category="optional",severity="low"} 0
    zypper_applicable_patches{category="optional",severity="unspecified"} 0
    zypper_applicable_patches{category="feature",severity="critical"} 0
    zypper_applicable_patches{category="feature",severity="important"} 0
    zypper_applicable_patches{category="feature",severity="moderate"} 0
    zypper_applicable_patches{category="feature",severity="low"} 0
    zypper_applicable_patches{category="feature",severity="unspecified"} 0
    zypper_applicable_patches{category="document",severity="critical"} 0
    zypper_applicable_patches{category="document",severity="important"} 0
    zypper_applicable_patches{category="document",severity="moderate"} 0
    zypper_applicable_patches{category="document",severity="low"} 0
    zypper_applicable_patches{category="document",severity="unspecified"} 0
    zypper_applicable_patches{category="yast",severity="critical"} 0
    zypper_applicable_patches{category="yast",severity="important"} 0
    zypper_applicable_patches{category="yast",severity="moderate"} 0
    zypper_applicable_patches{category="yast",severity="low"} 0
    zypper_applicable_patches{category="yast",severity="unspecified"} 0
    # HELP zypper_service_needs_restart Set to 1 if service requires a restart due to using no-longer-existing libraries.
    # TYPE zypper_service_needs_restart gauge
    zypper_service_needs_restart{service="nscd"} 1
    zypper_service_needs_restart{service="dbus"} 1
    zypper_service_needs_restart{service="cups"} 1
    zypper_service_needs_restart{service="sshd"} 1
    zypper_service_needs_restart{service="cron"} 1
    # HELP zypper_product_end_of_life Unix timestamp on when support for the product will end.
    # TYPE zypper_product_end_of_life gauge
    zypper_product_end_of_life{product="openSUSE"} 1606694400
    zypper_product_end_of_life{product="openSUSE_Addon_NonOss"} 1000000000000001
    # HELP zypper_needs_rebooting Whether the system requires a reboot as core libraries or services have been updated.
    # TYPE zypper_needs_rebooting gauge
    zypper_needs_rebooting 0
    # HELP zypper_scrape_success Whether the last scrape for zypper data was successful.
    # TYPE zypper_scrape_success gauge
    zypper_scrape_success 1

To get this picked up by the `Prometheus Node Exporter's`_ textfile collector dump the output into a ``zypper.prom`` file in the textfile collector directory.
You can utilise the ``--output-file`` parameter to have the exporter write directly to that file.::

    > zypper-patch-status-collector --output-file /var/lib/node_exporter/collector/zypper.prom

Installation
------------

Running this requires Python.

Install as any Python software via pip::

    pip install zypper-patch-status-collector

It also requires the reboot advisory and the lifecycle plug-in for zypper to be installed::

    zypper install zypper-needs-restarting zypper-lifecycle-plugin

Tests
-----

The tests are based on pytest_.
Just run the following in the project root::

    pytest

The default configuration requires the following Python packages to be available:

* `pytest`
* `pytest-cov`
* `pytest-mock`

License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You can find a full version of the license in the `LICENSE file`_.
If not, see https://www.gnu.org/licenses/.


.. _`Prometheus Node Exporter's`: https://github.com/prometheus/node_exporter
.. _pytest: https://docs.pytest.org/en/latest/
.. _`LICENSE file`: ./LICENSE.txt
